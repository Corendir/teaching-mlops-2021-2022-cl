import unittest
import main

class My_test_class(unittest.TestCase):

    def test_intent_inference(self):
        exit = main.intent_inference()
        self.assertTrue(type(exit) == "<class 'str'>")   
     
if __name__ == '__main__':
    unittest.main()        
