FROM python:3.7-buster

MAINTAINER Louis Lavenseau <llavenseau@ensc.fr>

COPY . .
RUN pip install -r requirements.txt

CMD ["python3", "main.py"]
